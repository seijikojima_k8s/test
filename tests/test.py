import unittest

def add(x, y):
    return x + y

def sub(x,y):
    return x - y

class TestCalc(unittest.TestCase):

    def test_add_three(self):
        self.assertEqual(add(5,3), 8)

    def test_add_three_fail(self):
        self.assertEqual(sub(5,3), 2)